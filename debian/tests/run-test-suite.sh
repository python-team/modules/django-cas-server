#!/bin/bash
set -efu

cp -a cas_server pytest.ini setup.cfg ${AUTOPKGTEST_TMP}

for py3vers in $(py3versions -s); do
    echo
    echo "***************************"
    echo "*** Testing with ${py3vers}"
    echo "***************************"
    echo
    cd ${AUTOPKGTEST_TMP} && \
        echo -e "Content of current working folder:\n" && \
        ls -la && \
        echo -e "Running tests...\n" && \
        PYTHONPATH=. "${py3vers}" -m pytest -sv && \
        rm -rf .pytest_cache || exit 1
done

echo

exit 0
